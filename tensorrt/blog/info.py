import tensorflow as tf
from tensorflow.python.platform import gfile


INPUT_FILE = 'resnet_v2_fp32_savedmodel_NCHW_jpg/1538687370/saved_model.pb'
OUTPUT_FILE = '/tmp/log'

graph = tf.get_default_graph()
graphdef = graph.as_graph_def()
graphdef.ParseFromString(gfile.FastGFile(INPUT_FILE, "rb").read())
_ = tf.import_graph_def(graphdef, name="")
summary_write = tf.summary.FileWriter(OUTPUT_FILE , graph)
